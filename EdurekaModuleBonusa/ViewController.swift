//
//  ViewController.swift
//  EdurekaModuleBonusa
//
//  Created by Jay on 06/03/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit
import CloudKit

class ViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Load()
    {
        print("Load function has started")
        
        //let myContainer = CKContainer.default()
        //iCloud.com.thechalakas.EdurekaModuleBonusb
        
        //get the container from the cloud dashboard
        let myContainer = CKContainer(identifier: "iCloud.com.thechalakas.EdurekaModuleBonusb")
        //get the database we are working with
        let publicDatabase = myContainer.publicCloudDatabase
        //get the zone
        let recordZone = CKRecordZone(zoneName: "_defaultZone")
        
        //set the predicate without this query will not run
        let predicate = NSPredicate(value: true)
        
        //setup the query
        let query = CKQuery(recordType: "CloudCat", predicate: predicate)
        
        //execute the actual query
        publicDatabase.perform(query, inZoneWith: recordZone.zoneID,
                                 completionHandler: ({results, error in
                                    
                                    if (error != nil)
                                    {
                                        print(error!.localizedDescription)
                                    
                                    }
                                    else
                                    {
                                    
                                        if results!.count > 0
                                        {
                                            for record: CKRecord in results!
                                            {
                                                //print the record that we obtained
                                                print(record)
                                            }

                                        }
                                        else
                                        {
                                                    print("No record matching the address was found")
                                        }
                                    }
                                    }
                                 ))
        
        print("Load function has started")
    }

    @IBAction func Save()
    {
        print("Save function has started")
        
        //check comments in the load function
        let myContainer = CKContainer(identifier: "iCloud.com.thechalakas.EdurekaModuleBonusb")
        let publicDatabase = myContainer.publicCloudDatabase
        let recordZone = CKRecordZone(zoneName: "_defaultZone")
        
        //create a record object
        let myRecord = CKRecord(recordType: "CloudCat",
                                zoneID: (recordZone.zoneID))
        //set the values of the object to be pushed to the cloud
        myRecord.setObject("Cat From App" as CKRecordValue, forKey: "Name")
        myRecord.setObject(6 as CKRecordValue, forKey: "Age")
        
        //setup the operation for the cloud
        let modifyRecordsOperation = CKModifyRecordsOperation(
            recordsToSave: [myRecord],
            recordIDsToDelete: nil)
        
        //callback function when the operation is completed.
        modifyRecordsOperation.modifyRecordsCompletionBlock =
            { records, recordIDs, error in
                if (error != nil)
                {
                        print(error!.localizedDescription)
                }
                else
                {
                    print("success")
                }
        }
        
        //execute the save function. 
        publicDatabase.add(modifyRecordsOperation)
                
        print("Save function has started")
    }


}

